import os, time # For path management and keeping track of timing.

from openpyxl import load_workbook
import matplotlib.pyplot as plt
import matplotlib.cm as cm # For color mapping
import numpy as np
# Good examples: https://automatetheboringstuff.com/chapter12/

from Plastic_collapse_analyze import PC_analyze, Single_PC_analysis
from Tensile_analyze import Tensile_analyze, Single_Tensile_analysis
from PC_power_analysis import POW_analyze, Single_POW_analysis

start = time.clock() # For timing the whole analysis

homepath = os.path.abspath("./") # Find absolute path of current directory (necessary for opening spreadsheets)


PCdata = [] # Where the data ends up being stored for a summary plot
plotnumber = 0 # Keeps track of how many data points to plot
datacount = 0 # For limiting looping during development


matlist = ["Al","Cu","Sn"] # A short list of the materials we're working with.

PCtxt_path = os.path.join(homepath,"Batch_results","POW","pcPOW"+".txt")
PCtxt = open(PCtxt_path,"w")
PCtxt.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\n".format( # This code mirrors the actual data recording to make TDS .txt file easier to modify 
    "Material","Thickness","Specimen type","Experiment name",
    "Actual thickness","Final width","Initial width","Final a/w",
    "mFit m","mFit var","pcPow int","pcPow exp","pcPow int ivar","pcPow exp nvar","Number of propagation points"))

for material in os.listdir(homepath): # Searching root (zero levels deep)
    matpath = os.path.join(homepath,material)
    if os.path.isdir(matpath) and material != ".hg" and material in matlist:  
        dsavepath = os.path.join(homepath,"Batch_results","POW","{}.txt".format(material)) # Place to save output from analysis
        RAWtxt = open(dsavepath,'w') # We can open in write mode since this will encompass all of the data   
        for thickness in os.listdir(matpath): # Searching 1 level deep (material)
            thickpath = os.path.join(matpath,thickness)
            if os.path.isdir(thickpath):
                for spectype in os.listdir(thickpath): # Searching 2 levels deep (thickness)
                    specpath = os.path.join(thickpath,spectype)                    
                    if os.path.isdir(specpath):
                        for experiment in os.listdir(specpath): # Searching 3 levels deep (specimen type)
                            exppath = os.path.join(specpath,experiment)
                            # INSERT IF STATEMENT HERE TO DO TENSILE ANALYSIS
                            # ACTUALLY, YOU CAN STILL CHECK THE EXPERIMENT TYPE IN THE DEEPEST LAYER OF THESE NESTED LOOPS!
                            if os.path.isdir(exppath):
                                for data in os.listdir(exppath): # Searching 4 levels deep (individual experiments)
                                    datapath = os.path.join(exppath,data)
                                    if (data[-5:] == ".xlsx" or data[-4:] == ".xls") and data[0] != "~": # Program doesn't work with xls, excludes files with "~" in it                                        
                                        #print datapath # By this point, the program has found all of the .xls and .xlsx files, PUT ANALYSES HERE
                                        print "\n", datapath
                                        if spectype == "Tensile":
                                            results = Tensile_analyze(datapath)
                                            #analysispathlist.append(datapath)
                                            if results != "NO DATA":   
                                                print "Successfully loaded tensile data"
                                                Tresult = Single_Tensile_analysis(datapath)
                                                PCtxt.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\n".format(
                                                    material,thickness,spectype,experiment,
                                                    Tresult["Thickness"],"N/A","N/A","N/A",
                                                    Tresult["Ultimate strength"]/1000000,"N/A","N/A","N/A","N/A","N/A","N/A")) # UTS gets converted to MPa.

                                                # Write raw data to a log file for later visualization
                                                RAWtxt.write("{}\t{}\t{}\t{}\n".format(material,thickness,spectype,experiment))
                                                RAWtxt.write("a/w\t0\n")
                                                RAWtxt.write("Stress\t{}\n".format(Tresult["Ultimate strength"]/1000000))

                                                print "T results: {}".format(Tresult)
                                            else: print "Tensile data not found"
                                            print "\n"
                                        else: # Crack growth experiments
                                            results = POW_analyze(datapath)
                                            #analysispathlist.append(datapath)
                                            if results != "NO DATA": 
                                                # Write raw data to a log file for later visualization
                                                RAWtxt.write("{}\t{}\t{}\t{}\n".format(material,thickness,spectype,experiment))
                                                RAWtxt.write("a/w")
                                                for item in results["AW"]: RAWtxt.write("\t{}".format(item))
                                                RAWtxt.write("\nStress")
                                                for item in results["Stress"]: RAWtxt.write("\t{}".format(item))
                                                RAWtxt.write("\n")

                                                print "Successfully loaded plastic collapse data"
                                                #plt.scatter(results["AW"],results["Stress"])	# DEPRECATED - old way of plotting collected data											
                                                PCdata.append(results) # For later plotting
                                                PCresult = Single_POW_analysis(datapath)
                                                print "PC results: {}".format(PCresult)
                                                PCtxt.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\n".format(
                                                    material,thickness,spectype,experiment,
                                                    PCresult["Thickness"],PCresult["Final width"],PCresult["Initial width"],PCresult["Final a/w"],
                                                    PCresult["mFit m"],PCresult["mFit var"],PCresult["pcPow int"],PCresult["pcPow exp"],PCresult["pcPow int var"],PCresult["pcPow exp var"],PCresult["Propagation points"]))                                              
                                            else: print "Crack data not found"
                                            print "\n"
        RAWtxt.close() # CLose the file where we're putting data

PCtxt.close() # Close results file
print "Analysis time: ", time.clock()-start # Tell user how long it took


plotquestion = raw_input("Batch analysis concluded. Plot collected data? (y/n)")
if plotquestion == "y":
	plt.close('all')
	print "Plotting results."
	colors = cm.Paired(np.linspace(0, 1, len(PCdata)))

	print "Dataset length", len(PCdata)

	for results in PCdata:
		plt.scatter(results["AW"],results["Stress"], c = colors[plotnumber])
		plotnumber = plotnumber + 1

	plt.show()