import xlrd # needed for xls files
from openpyxl.workbook import Workbook 
from openpyxl.reader.excel import load_workbook, InvalidFileException # For opening xlsx files
import openpyxl

import matplotlib.pyplot as plt

import numpy as np
from scipy.optimize import curve_fit
from scipy.stats.distributions import  t

import time # for using dates in saved files
import os # for managing paths

# Good examples of Excel automation: https://automatetheboringstuff.com/chapter12/


### PLASTIC COLLAPSE CODE FOR CRACK GROWTH EXPERIMENTS
def PC_analyze(datapath): # This hunts through one of our xlsx files and finds plastic collapse data (a/w and stress)
    if datapath[-5:] == ".xlsx":
        wb = load_workbook(filename = datapath, data_only=True)
    elif datapath[-4:] == ".xls":
        wb = open_xls_as_xlsx(datapath)
    else: return "NO DATA"

    sheetlist = wb.get_sheet_names()
    #print sheetlist

    if 'Crack_length' in sheetlist:
        CL_analysis = wb['Crack_length']
        #print "Sheet size is {} rows by {} columns".format(CL_analysis.max_row,CL_analysis.max_column)
        for row in CL_analysis.rows: # Scans through sheet to find the titles of desired data
            for cell in row:               
                if isinstance(cell.value, (str, unicode)):
                    if isinstance(cell.column,str): column = openpyxl.utils.column_index_from_string(cell.column) # To fix issue where .xls imports have letter column names
                    else: column = cell.column 

                    if len(cell.value) >= 12: 
                        if cell.value[:12] == "Crack length": CL_header = (cell.row,column) 
                    if len(cell.value) >= 10:  
                        if cell.value[:10] == "Width (mm)": Width_header = (cell.row,column) 
                    if len(cell.value) >= 14:  
                        if cell.value[:14] == "Nominal stress": Stress_header = (cell.row,column) 
                    if len(cell.value) >= 4: # This presumes that the crack length is already computed for a M(T) specimen
                        if cell.value[:6] == "2a/(w)" or cell.value[:4] == "2a/w": AW_header = (cell.row,column) 
                    if len(cell.value) >= 3: # This presumes that the crack length is already computed for a SE(T) specimen
                        if cell.value[:3] == "a/w": AW_header = (cell.row,column)
                    if len(cell.value) >= 9: # Finding the recorded thickness 
                        if cell.value[:9] == "Thickness": Thickness_label = (cell.row,column)
        #print CL_header, Width_header, Stress_header, AW_header, Thickness_label

        try: 
            # Now that we found all of the desired data, we have to load it into memory.
            CL_data, Width_data, Stress_data, AW_data = [], [], [], []
            for i in range(CL_header[0]+1, CL_analysis.max_row):
                CL_val = CL_analysis.cell(row=i, column=CL_header[1]).value                

                if CL_val != 0 and CL_val != None and not isinstance(CL_val, (str, unicode)): # We only want nonzero numerical values.
                    Width_val = CL_analysis.cell(row=i, column=Width_header[1]).value
                    Stress_val = CL_analysis.cell(row=i, column=Stress_header[1]).value
                    AW_val = CL_analysis.cell(row=i, column=AW_header[1]).value # This presumes that the crack length is already computed
                    Thickness_val = CL_analysis.cell(row=Thickness_label[0], column=(Thickness_label[1]+1)).value # Thickness is reported one cell to the right of the label

                    CL_data.append(CL_val)
                    Width_data.append(Width_val)
                    Stress_data.append(Stress_val)
                    AW_data.append(AW_val)
            results = { "CL" : CL_data,
                        "Width" : Width_data,
                        "Stress" : Stress_data,
                        "AW" : AW_data,
                        "Thickness" : Thickness_val
                        }
            return results
        except UnboundLocalError: return "NO DATA" #In case attempt to find headers fails
    else: return "NO DATA"

#To consider: Different fitting methods may be more appropriate
def PC_fit(xdata, ydata): # This identifies the propagation part of a collapse analysis and does a least squares fit to it
    #We must force the curve fit through the point (1,0). 
    def func(x, a): # linear function with slope a to fit to data
        return a*(1-x)

    # Fit data
    initial_guess = [ydata[0]]
    pars, pcov = curve_fit(func, xdata, ydata, p0=initial_guess)

    results = { "mFit m" : pars[0],#(-1)*pars[0],
                "mFit var" : np.diag(pcov)[0]
                }
    return results
    
def lsLinFit(xdata, ydata): # This does a least squares to whatever data you give it it in full slope-intercept form.
    def func(x, m, b): # linear function with slope a to fit to data
        return m * x + b
    # Fit data
    if xdata[-1]-xdata[0] != 0: slopest = (ydata[-1]-ydata[0])/(xdata[-1]-xdata[0]) # Prevents divide by zero error.
    else: slopest = 0
    intest = ydata[0]-(slopest*xdata[0])

    initial_guess = [slopest, intest]
    pars, pcov = curve_fit(func, xdata, ydata, p0=initial_guess)

    #print pars, pcov
    results = { "m" : pars[0],
                "b" : pars[1],
                "mvar" : np.diag(pcov)[0],
                "bvar" : np.diag(pcov)[1]
                }
    return results

    # To be commented out for most uses: some plotting stuff
    # Mostly based on http://kitchingroup.cheme.cmu.edu/blog/2013/02/12/Nonlinear-curve-fitting-with-parameter-confidence-intervals/
    '''
    ### Student's t-test computations
    alpha = 0.05 # 95% confidence interval = 100*(1-alpha)
    n = len(stress_prop)    # number of data points
    p = len(pars) # number of parameters
    dof = max(0, n - p) # number of degrees of freedom
    # student-t value for the dof and confidence level
    tval = t.ppf(1.0-alpha/2., dof) 
    for i, p,var in zip(range(n), pars, np.diag(pcov)):
        sigma = var**0.5
        print 'p{0}: {1} [{2}  {3}]'.format(i, p,
                                      p - sigma*tval,
                                      p + sigma*tval)

    print "Intercept computed: {}".format(-1*pars[0])

    plt.plot(AW_shifted,stress_prop,'bo ', label='data')
    xfit = np.linspace(-1,0)
    yfit = func(xfit, pars[0])
    plt.plot(xfit,yfit,'b-', label='fit')

    # Computing confidence intervals
    lower = []
    upper = []
    for p,var in zip(pars, np.diag(pcov)):
        sigma = var**0.5
        lower.append(p - sigma*tval)
        upper.append(p + sigma*tval)

    fitlow = func(xfit, *lower)
    plt.plot(xfit,fitlow,'--', color='red')
    fithi = func(xfit, *upper)
    plt.plot(xfit,fithi,'--', color='red', label='CI 95%')
    plt.legend(numpoints = 2,loc='best')
    plt.show()
    '''

def Single_PC_analysis(path): # Takes in path to xlsx file. Does collapse analysis, saves fig and data at path, returns data
    # We might want to know when this analysis was run.
    analysistime = str(time.strftime("%d"))+time.strftime("%b")+str(time.strftime("%Y"))+"_"+str(time.strftime("%H"))+"."+str(time.strftime("%M"))+"."+str(time.strftime("%S"))

    results = PC_analyze(path) # Pull the data we need from the excel file
    awdata, stressdata, CLdata, widthdata = results["AW"], results["Stress"], results["CL"], results["Width"] # Extract the data we need to fit

    # Distinguish between initiation and propagation.
    for i in range(0, len(stressdata)): # Separate the initiation portion of the curve from the propagation portion
        if stressdata[i] == max(stressdata):            
            CIDX = i
            break
    if 'CIDX' not in locals() and 'CIDX' not in globals(): 
        CIDX = 0 # In case propagation search somehow failed, send value so analysis can continue
        print "CMOD_analyze warning: initiation/propagation transition not found."

    try: mFitresults = PC_fit(awdata[CIDX:],stressdata[CIDX:]) # Fit the data with a least squares fit forced through (1,0)
    except: 
        print "Plastic_collapse_analyze WARNING: 1-parameter fit failed"
        mFitresults = {"mFit m" : 0, "mFit var" : 0}
    try: mbFitresults = lsLinFit(awdata[CIDX:],stressdata[CIDX:]) # Fit the data with a least squares fit not forced to have a particular intercept (in case the intercept is meaningful)
    except:
        print "Plastic_collapse_analyze WARNING: 2-parameter fit failed"
        mbFitresults = {"m" : 0, "b" : 0, "mvar" : 0, "bvar" : 0}

    # Set up plots of data and fits. 
    fig = plt.figure(figsize=(18,8))
    fig.suptitle("Plastic collapse analysis B = {} um {}\n{}".format(results["Thickness"],analysistime,os.path.dirname(path)))  

    # Set up first data plot.
    ax = fig.add_subplot(1,2,1)    
    ax.set_xlabel("Normalized crack length a/w (SE) or 2a/w (M)")
    ax.set_ylabel("Stress (MPa)")
    ax.scatter(awdata[:CIDX],stressdata[:CIDX], c = 'blue', label = 'initiation')
    ax.scatter(awdata[CIDX:],stressdata[CIDX:], c = 'red', label = 'propagation')

    # Plot single-parameter fit.
    ax.set_title("Single-parameter fit through (0,1)")
    def mFunc(x, m): # linear function with slope a to fit to data
        return m*(1-x) 
    xfit = np.linspace(0,1)
    yfit = mFunc(xfit, mFitresults["mFit m"])
    ax.plot(xfit,yfit,'b-', label="Fit with (-m)=b= {} MPa".format(str(mFitresults["mFit m"])[:6]))
    ax.plot(xfit,mFunc(xfit, mFitresults["mFit m"]+np.sqrt(mFitresults["mFit var"])),'--',color='red', label='m STDV {} MPa'.format(str(np.sqrt(mFitresults["mFit var"]))[:6]))
    ax.plot(xfit,mFunc(xfit, mFitresults["mFit m"]-np.sqrt(mFitresults["mFit var"])),'--',color='red')
    ax.legend(numpoints = 2,loc='best')

    # Set up first data plot.
    ax = fig.add_subplot(1,2,2)    
    ax.set_xlabel("Normalized crack length a/w (SE) or 2a/w (M)")
    ax.set_ylabel("Stress (MPa)")
    ax.scatter(awdata[:CIDX],stressdata[:CIDX], c = 'blue', label = 'initiation')
    ax.scatter(awdata[CIDX:],stressdata[CIDX:], c = 'red', label = 'propagation')

    # Plot slope-intercept fit.
    ax.set_title("Slope-intercept fit")
    def mbFunc(x, m, b): # linear function with slope a to fit to data
        return m*x+b 
    xfit = np.linspace(0,1)
    yfit = mbFunc(xfit, mbFitresults["m"], mbFitresults["b"])
    ax.plot(xfit,yfit,'b-', label="Fit with (m,b) = ({},{}) MPa".format( str(mbFitresults["m"])[:6] , str(mbFitresults["b"])[:6] ) )
    ax.plot(xfit,mbFunc(xfit, mbFitresults["m"], mbFitresults["b"]+np.sqrt(mbFitresults["bvar"])),'--',color='red', label='b STDV {} MPa'.format(str(np.sqrt(mbFitresults["bvar"]))[:6]))
    ax.plot(xfit,mbFunc(xfit, mbFitresults["m"], mbFitresults["b"]-np.sqrt(mbFitresults["bvar"])),'--',color='red')
    ax.legend(numpoints = 2,loc='best')

    # Save and/or show figure.
    figdir = os.path.join(os.path.dirname(path),"PCanalysis"+".pdf")
    fig.savefig(figdir)
    #plt.show()  # Make sure this is commented out before using in a loop processing

    textdir = os.path.join(os.path.dirname(path),"PCanalysis"+".txt")
    PCnotes = open(textdir,"w")
    PCnotes.write("Analysis time: {0}\tPropagation data points: {1}\tmFit m: {2}\tmFit var: {3}\tmbFit m: {4}\tmbFit b: {5}\tmbFit mvar: {6}\tmbFit bvar: {7}".format(
        analysistime,len(awdata[CIDX:]),mFitresults["mFit m"],mFitresults["mFit var"],mbFitresults["m"],mbFitresults["b"],mbFitresults["mvar"],mbFitresults["bvar"]))
    PCnotes.write("{}\n".format(zip(awdata,stressdata)))
    PCnotes.close()

    results = { "mFit m" : mFitresults["mFit m"],
                "mFit var" : mFitresults["mFit var"],
                "mbFit m" : mbFitresults["m"],
                "mbFit b" : mbFitresults["b"],
                "mbFit mvar" : mbFitresults["mvar"],
                "mbFit bvar" : mbFitresults["bvar"],
                "Propagation points" : len(awdata[CIDX:]), # 'CIDX' is the coordinate of the maximum stress, this gives it and all subsequent points
                "Analysis time" : analysistime,
                "Thickness" : results["Thickness"],
                "Final a/w" : awdata[-1], 
                "Final width" : widthdata[-1],                
                "Initial width" : widthdata[0]
               }
    return results 
  
def open_xls_as_xlsx(filename):
    # first open using xlrd
    XLSworkbook = xlrd.open_workbook(filename)
    XLSXbook = Workbook()

    for sheetname in XLSworkbook.sheet_names():
        XLSXbook.create_sheet(sheetname)
        XLSXsheet =XLSXbook[sheetname]

        XLSsheet = XLSworkbook.sheet_by_name(sheetname)
        nrows = XLSsheet.nrows
        ncols = XLSsheet.ncols
        #print "Opened sheet {} with dimensions {} rows by {} columns".format(sheetname,nrows,ncols)

        for row in range(0, nrows):
            for col in range(0, ncols):
                cell = XLSsheet.cell_value(row, col)
                XLSXsheet.cell(row=(row+1), column=(col+1)).value = cell # openpyxl requires rows and cols of value >= 1, xlrd starts at 0
                #if isinstance(cell,unicode):
                #    try: XLSXsheet.cell(row=(row+1), column=(col+1)).value = str(cell) # Later code chokes on unicode, so I convert it to strings when needed.
                #    except ValueError: XLSXsheet.cell(row=(row+1), column=(col+1)).value = cell
                #else:
                #    XLSXsheet.cell(row=(row+1), column=(col+1)).value = cell # openpyxl requires rows and cols of value >= 1, xlrd starts at 0

    for sheetname in XLSXbook.get_sheet_names(): # The code tends to create an extra, useless sheet. This gets rid of it.
        if sheetname == "Sheet": XLSXbook.remove_sheet(XLSXbook.get_sheet_by_name("Sheet"))

    #XLSXbook.save("pconvert_test.xlsx") # In case we need to look at the converted copy
    return XLSXbook
 
'''
start = time.clock()
#devpath = 'Z:\Repositories_WRL\Experiment_Planner_WRL\Excel_dev\SE1.xlsx'
devpath = 'Z:\Repositories_WRL\Experiment_Planner_WRL\Excel_dev\SE1.xls'
#devpath = 'Z:\Repositories_WRL\Experiment_Planner_WRL\Excel_dev\T1_fitcheck.xlsx'
#devpath = 'Z:\Repositories_WRL\Experiment_Planner_WRL\Excel_dev\\19May2017_Test1_Sn_B100_SE1.0.xlsx' # For some reason file names sometimes need a double slash \\
print devpath
print Single_PC_analysis(devpath)
print "Analysis time: ", time.clock()-start
'''