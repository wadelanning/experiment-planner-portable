import xlrd # needed for xls files
from openpyxl.workbook import Workbook 
from openpyxl.reader.excel import load_workbook, InvalidFileException # For opening xlsx files
import openpyxl

import matplotlib.pyplot as plt
from textwrap import wrap # For making big titles fit.

import numpy as np
from scipy.optimize import curve_fit
from scipy.stats.distributions import  t

import time # for using dates in saved files
import os # for managing paths

# Good examples: https://automatetheboringstuff.com/chapter12/


### TENSILE ANALYSYS CODE FOR TENSILE EXPERIMENTS
def Tensile_analyze(datapath): # This hunts through one of our xlsx files and finds the data we want (in this case stress)
    if datapath[-5:] == ".xlsx":
        wb = load_workbook(filename = datapath, data_only=True)
        #print "Opened .xlsx sheet"
    elif datapath[-4:] == ".xls":
        wb = open_xls_as_xlsx(datapath)
        #print "Opened .xls sheet"
    else: return "NO DATA"

    sheetlist = wb.get_sheet_names()
    #print sheetlist
    if "F-D" in sheetlist: sheetname = "F-D" # We were a bit inconsistent naming sheets.
    else: sheetname = "Sheet1"

    if sheetname in sheetlist:
        FD_analysis = wb[sheetname]
        #print "Sheet size is {} rows by {} columns".format(FD_analysis.max_row,FD_analysis.max_column)
        Pos_header = Force_header = Time_header = Stress_header = Thickness_header = "NOT FOUND" # All of these will be overwritten if the correct header names are found
        for row in FD_analysis.rows: # Scans through sheet to find the titles of desired data
            for cell in row:               
                if isinstance(cell.value, (str, unicode)):
                    if isinstance(cell.column,str): column = openpyxl.utils.column_index_from_string(cell.column) # To fix issue where .xls imports have letter column names
                    else: column = cell.column 

                    if len(cell.value) >= 13: 
                        if cell.value[:13] == "Position (mm)": Pos_header = (cell.row,column) # 'Position control' can get confused with 'Position (mm)' be careful
                    if len(cell.value) >= 4:  
                        if cell.value[:4] == "Load": Force_header = (cell.row,column) 
                    if len(cell.value) >= 4:  
                        if cell.value[:4] == "Time": Time_header = (cell.row,column) 
                    if len(cell.value) >= 6: 
                        if cell.value[:6] == "Stress": Stress_header = (cell.row,column) # This presumes stress has already been computed.
                    if len(cell.value) >= 9: 
                        if cell.value[:9] == "Thickness": Thickness_header = (cell.row,column)                    
        #print Pos_header, Force_header, Time_header, Stress_header, Thickness_header

        try: 
            # Now that we found all of the desired data, we have to load it into memory.
            Pos_data, Force_data, Time_data, Stress_data = [], [], [], []
            if Thickness_header != "NOT FOUND":
                thickness = FD_analysis.cell(row=Thickness_header[0], column=(Thickness_header[1]+1)).value # Thickness is stored 1 cell to the right of label
            else: thickness = "NOT FOUND"
            for i in range(Pos_header[0]+1, FD_analysis.max_row):
                Pos_val = FD_analysis.cell(row=i, column=Pos_header[1]).value
                if Pos_val != 0 and Pos_val != None and not isinstance(Pos_val, (str, unicode)) : # We only want nonzero numerical values. (This is a leftover I'm leaving in case we need a criteria to eliminate data.)             
                    Pos_data.append(Pos_val) # Append data

                    Force_val = FD_analysis.cell(row=i, column=Force_header[1]).value
                    Time_val = FD_analysis.cell(row=i, column=Time_header[1]).value
                    Stress_val = FD_analysis.cell(row=i, column=Stress_header[1]).value # This presumes that the stress

                    Force_data.append(Force_val)
                    Time_data.append(Time_val)
                    Stress_data.append(Stress_val)

            results = { "Position" : Pos_data,
                        "Force" : Force_data,
                        "Time" : Time_data,
                        "Stress" : Stress_data,
                        "Thickness" : thickness
                        }
            return results
        except UnboundLocalError: return "NO DATA" #In case attempt to find headers fails
    else: return "NO DATA"

def Single_Tensile_analysis(path): # Takes in path to xlsx file. Does collapse analysis, saves fig and data at path, returns data
    # We might want to know when this analysis was run.
    analysistime = str(time.strftime("%d"))+time.strftime("%b")+str(time.strftime("%Y"))+"_"+str(time.strftime("%H"))+"."+str(time.strftime("%M"))+"."+str(time.strftime("%S"))

    results = Tensile_analyze(path) # Pull the data we need from the excel file
    if results == "NO DATA": return "NO DATA"

    pos_data = results["Position"] # Extract the data we need
    force_data = results["Force"]
    stress_data = results["Stress"]

    disp_data = [] # Compute displacement
    for i in xrange(0,len(pos_data)):         
        #print i, type(pos_data[i]), pos_data[i] != None
        disp_data.append(pos_data[i]-pos_data[0])

    work_data = [0] # Compute work done on specimen
    for i in xrange(0,len(disp_data)-1): 
        work_data.append((1.0/1000)*(pos_data[i+1]-pos_data[i])*(force_data[i+1]+force_data[i])/2 + work_data[i]) # Includes conversion because position is in mm

    try: 
        ult_strength = max(stress_data) # Find ultimate tensile strength
        for i in range(0, len(stress_data)): # Find relevant data which corresponds to the maximum stress.
            if stress_data[i] == ult_strength:
                disp_maxS = disp_data[i]
                work_maxS = work_data[i]
                disp_maxS = disp_data[i]
                break

        plt.clf() # In case there's leftover stuff from other plots.

        plt.suptitle("B = {} um {} {}".format(results["Thickness"],analysistime,os.path.dirname(path)) )
  
        plt.subplot(1,2,1)
        plt.title("Tensile Stress-disp analysis")
        plt.xlabel("Displacement (mm)")
        plt.ylabel("Stress (Pa)")
        plt.scatter(disp_data,stress_data, c = 'blue', label = 'Stress vs. displacement')
        plt.plot(disp_data,[ult_strength]*len(disp_data), c = 'red', label = "Ultimate strength {} MPa".format(ult_strength/1000000)) # Show reference line
        plt.legend(numpoints = 2,loc='best')

        plt.subplot(1,2,2)
        plt.title("Tensile work analysis")
        plt.xlabel("Displacement (mm)")
        plt.ylabel("Work (J)")
        plt.scatter(disp_data,work_data, c = 'green', label = 'Work vs. displacement')
        plt.plot(disp_data,[work_maxS]*len(disp_data), c = 'red') # Show where the UTS spot is
        plt.plot([disp_maxS]*len(disp_data),work_data, c = 'red') 

        figdir = os.path.join(os.path.dirname(path),"SD_analysis"+".pdf")
        plt.savefig(figdir)
        #plt.show()  # Make sure this is commented out before using in a loop processing

        textdir = os.path.join(os.path.dirname(path),"Tensile"+".txt")
        Tnotes = open(textdir,"w")
        Tnotes.write("Analysis time: {}\tUltimate strength: {}\n".format(analysistime,ult_strength))
        Tnotes.close()

    except: # Some data causes errors
        print "WARNING: Something went wrong with tensile analysis."
        ult_strength = 0 # Find ultimate tensile strength
        disp_maxS = 0
        work_maxS = 0
        disp_maxS = 0

    results = { #"Position" : pos_data,
                #"Displacement" : disp_data,
                #"Force" : force_data,
                #"Stress" : stress_data,
                "Ultimate strength": ult_strength,
                "Analysis time" : analysistime,
                "Thickness" : results["Thickness"],
                "Work to max stress" : work_maxS,
                "Work to failure" : work_data[-1]
               }
    return results 

# XLS CONVERSION COMMAND
def open_xls_as_xlsx(filename):
    # first open using xlrd
    XLSworkbook = xlrd.open_workbook(filename)
    XLSXbook = Workbook()

    for sheetname in XLSworkbook.sheet_names():
        XLSXbook.create_sheet(sheetname)
        XLSXsheet =XLSXbook[sheetname]

        XLSsheet = XLSworkbook.sheet_by_name(sheetname)
        nrows = XLSsheet.nrows
        ncols = XLSsheet.ncols
        #print "Opened sheet {} with dimensions {} rows by {} columns".format(sheetname,nrows,ncols)

        for row in range(0, nrows):
            for col in range(0, ncols):
                cell = XLSsheet.cell_value(row, col)
                XLSXsheet.cell(row=(row+1), column=(col+1)).value = cell # openpyxl requires rows and cols of value >= 1, xlrd starts at 0
                #if isinstance(cell,unicode):
                #    try: XLSXsheet.cell(row=(row+1), column=(col+1)).value = str(cell) # Later code chokes on unicode, so I convert it to strings when needed.
                #    except ValueError: XLSXsheet.cell(row=(row+1), column=(col+1)).value = cell
                #else:
                #    XLSXsheet.cell(row=(row+1), column=(col+1)).value = cell # openpyxl requires rows and cols of value >= 1, xlrd starts at 0

    for sheetname in XLSXbook.get_sheet_names(): # The code tends to create an extra, useless sheet. This gets rid of it.
        if sheetname == "Sheet": XLSXbook.remove_sheet(XLSXbook.get_sheet_by_name("Sheet"))

    #XLSXbook.save("pconvert_t est.xlsx") # In case we need to look at the converted copy
    return XLSXbook

'''
start = time.clock()
#devpath = 'Z:\Repositories_WRL\Experiment_Planner_WRL\Excel_dev\T1.xls'
#devpath = 'Z:\Repositories_WRL\Experiment_Planner_WRL\Excel_dev\T1.xlsx'
devpath = 'Z:\Repositories_WRL\Experiment_Planner_WRL\Excel_dev\March2017.xlsx'
#devpath = 'Z:\Repositories_WRL\Experiment_Planner_WRL\Excel_dev\SnB50.xls'
print Single_Tensile_analysis(devpath)
print "Analysis time: ", time.clock()-start
'''

