'''
Wade Lanning's experiment planner for Summer 2017
This program is meant to be put in a directory which will be a workplace for data storage and analysis.
It creates a "wish list" of subdirectories corresponding to different experiments I want to run.
It then checks those directories to see if any data has been put in them.
'''
experimentname = "Thickness effects on Mode I tearing of pure metal sheets"
elogname="Experiment_log.txt"
nlogname="Note_log.txt"

import os

# Each level of our project directory structure will correspond to a parameter in the experiment design
# Here, we put together a list of each level
# Level 0: Material
material = ["Al","Cu","Sn"]
# Level 1: Thickness - note that the thicknesses are unique for each material, so we have to manually specify them
Althick = [25.4,50.8,127]
Cuthick = [50.8,76.2,127]
Snthick = [25,50,100]
thickness = [Althick,Cuthick,Snthick]
# Level 2: Specimen types
specimen = ["T","SE","M"]
# Level 3: Crack length
cracklength = [1.0,2.0]

explist = [] # A list of directories, each one corresponding to a list we want to run

# This fills out the experiment list, with each level of the directory structure corresponding to a parameter in the experiment design
for zero in range(0,len(material)):
    for one in thickness[zero]:
        for two in specimen:            
            if two == "T":
                explist.append(os.path.join("{}".format(material[zero]),"B{}um".format(one),"{}ensile".format(two)))
                #print os.path.join("{}".format(material[zero]),"B{}um".format(one),"{}ensile".format(two))
            else:
                for three in cracklength:
                    explist.append( os.path.join("{}".format(material[zero]),"B{}um".format(one),"{}{}mm".format(two,three)))
                    #print os.path.join("{}".format(material[zero]),"B{}um".format(one),"{}{}mm".format(two,three))

print "Experiment plan for {}".format(experimentname)
print "List of planned experiments and whether or not they have been completed."


def refreshLogs(*args):
    expLog = open(elogname,"w") # A log of which experiment folders have or have not been populated
    noteLog = open(nlogname,"w") # A place to collect any notes left in "notes.txt" within each experiment folder

    for path in explist: # Iterates through all paths
        if not os.path.isdir(path):  # Create an experiment directory if it does not already exist
            os.makedirs(path)
            print "{} newly created".format(path) 
            expLog.write("{}\tnewly created\n".format(path)) 
        else: # Look inside the experiment directory so we can put whatever is in there into our logs
            contents = os.listdir(path)

            if "notes.txt" not in contents: # All directories should have a "notes.txt" files for assistants to leave notes in
                f = open(os.path.join(path,"notes.txt"),"w")
                f.close()

            nonnotes = []
            for i in contents: # We will only log things that are not our "notes.txt" files
                if i != "notes.txt":
                    nonnotes.append(i)
                        
            print "{0}: {1}".format(path,nonnotes)
            expLog.write("{}:\t{}\n".format(path,nonnotes)) # Records contents of the target path in logfile

        notepath = os.path.join(path,"notes.txt") # Open the notes files
        notefile = open(os.path.join(path,"notes.txt"),"r")
        noteLog.write("{}\n".format("_"*100)) # Create a place in the note log for any notes we find
        noteLog.write("{}\n".format(path))
        
        filedone = False
        lineNum = 0
        while not filedone: # Read all lines from note file and record them in the log
            currentline = notefile.read()
            if currentline != "":
                noteLog.write("{}\n".format(currentline))
            else:filedone = True
            
    expLog.close() # Close both log files in a tidy way.
    noteLog.close()

    os.startfile(elogname)
    os.startfile(nlogname)

refreshLogs()