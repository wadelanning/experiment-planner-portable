import xlrd # needed for xls files
from openpyxl.workbook import Workbook 
from openpyxl.reader.excel import load_workbook, InvalidFileException # For opening xlsx files
import openpyxl

import matplotlib.pyplot as plt
import matplotlib.cm as cm # For color mapping
from matplotlib.font_manager import FontProperties

import math
import numpy as np
from scipy.optimize import curve_fit
from scipy.stats.distributions import  t
from scipy.stats import f

import csv # For opening tab-delimited files

import time # for using dates in saved files
import os # for managing paths

# Lists of All of the original experiment parameters
# Materials
material = ["Al","Cu","Sn"]
matnum = 0 # Which material do we want to analyze?
# Level 1: Thickness
Althick = [25.4,50.8,127]
Cuthick = [50.8,76.2,127]
Snthick = [25,50,100]
thickness = [Althick,Cuthick,Snthick]
# Level 2: Specimen types
specimen = ["T","SE","M"]
# Level 3: Crack length
cracklength = [1.0]

# Here is where I trim things down if desired
#specimen = [specimen[1]]
Althick = [Althick[1]]
#Snthick = [Snthick[2]]
cracklength = [2.0]
specimen = ["SE","M"]

# Set up configurations we want to look at
# This will make a list of the configurations we planned
SpecConfigList = []
ThicknessList = []
for thickness in Althick:
    ThicknessList.append("B{}um".format(thickness))
    for config in specimen:
        if config == "T": SpecConfigList.append("Tensile")
        else: 
            for length in cracklength: SpecConfigList.append("{0}{1}mm".format(config,length))
# Show what  specimen configurations or thicknesses are going to be used
print "Configs: ",SpecConfigList
print "Thicknesses: ",ThicknessList


def Plot_PC(path,material): # Takes in path to xlsx file. Does collapse analysis, saves fig and data at path, returns data
    # We might want to know when this analysis was run.
    analysistime = str(time.strftime("%d"))+time.strftime("%b")+str(time.strftime("%Y"))+"_"+str(time.strftime("%H"))+"."+str(time.strftime("%M"))+"."+str(time.strftime("%S"))

    plt.figure(figsize=(16,10)) # Create a figure we will populate with our data

    FIT_data = []
    with open("{}\PCresults.txt".format(path), 'r') as inputfile:
        reader = csv.reader(inputfile, delimiter='\t')
        for row in reader:
            FIT_data.append(row)
    print "Data loaded from fits."
    print FIT_data.pop(0) # This shows the user the column headers and removes them from the list.
    def func(x, m, b): # linear slope-intercept function
        return m * x + b
    xfit = np.linspace(0,1) # The range of x values for plotting each fit

    # We need to match the raw data to the fits. 
    # I will make a nested list. [ [material, thickness, spectype, name]   ,[fitting data]   ] 
    fitlist = []
    for item in FIT_data: fitlist.append([[item[:4]],[item[4:] ]])    
    
    # Open the raw plastic collapse data
    PC_data = []
    with open("{}\{}.txt".format(path,material), 'r') as inputfile:
        reader = csv.reader(inputfile, delimiter='\t')
        for row in reader:
            PC_data.append(row)

    # Store relevant data for statistical comparisons
    statlist = []

    # Setting up plotting color coding
    #colors = cm.gist_ncar(np.linspace(0, 1, len(PC_data)/3)) # For large datasets - no duplicate colors
    colors = cm.prism(np.linspace(0, 1, len(PC_data)/3)) # For small datasets - high contrast but may duplicate
    #colors = cm.flag(np.linspace(0, 1, len(PC_data)/3)) # For true patriots

    for experiment in xrange(0,len(PC_data)/3): # This steps through every recorded experiment, which is composed of 3 lines
        # Here we set up conditional statements to decide what data to look at
        if PC_data[experiment*3][1] in ThicknessList and PC_data[experiment*3][2] in SpecConfigList:
            if PC_data[experiment*3][2][0] == "T": pmark = "D"
            elif PC_data[experiment*3][2][0] == "M": pmark = "s"
            else: pmark = "o"
            plt.scatter(PC_data[experiment*3+1][1:],PC_data[experiment*3+2][1:], c = colors[experiment], marker = pmark, label = PC_data[experiment*3])
            #vplt.scatter(PC_data[experiment*3+1][1:],PC_data[experiment*3+2][1:], c = colors[experiment], marker = pmark, label = PC_data[experiment*3], s = 100)

            for fitItem in FIT_data: # Find the row in the fit data which matches the data of interest, then plot the corresponding line.
                if PC_data[experiment*3][0] in fitItem and PC_data[experiment*3][1] in fitItem and PC_data[experiment*3][2] in fitItem and PC_data[experiment*3][3] in fitItem and "N/A" not in fitItem:
                    #Store the chosen data for later statistical comparisons
                    statlist.append(fitItem) 

                    yfit = func(xfit, float(fitItem[10]), float(fitItem[11])) # Create the fit line data
                    print fitItem[:4] # Show the specimen parameters
                    print "Actual thickness: ", fitItem[4]
                    print "m:", fitItem[10]
                    print "b:", fitItem[11]
                    print "mvar:", fitItem[12]
                    print "bvar:", fitItem[13]
                    plt.plot(xfit,yfit, c = colors[experiment])
                    # plt.plot(xfit,yfit, c = colors[experiment], linewidth=2)

    '''
    # DO STATISTICAL COMPARISONS
    # We need to be able to slice the data, so I'll convert it to a numpy array
    statlist = np.asarray(statlist)
    # Slope comparison
    print "**********************\nSlope Z-test"
    # Find max and min slopes
    for entry in statlist:
        if entry[10] == max(statlist[:,10]): mMax = entry
        if entry[10] == min(statlist[:,10]): mMin = entry

    print "Experiment with maximum slope: ", mMax[:4]
    print "Slope: {}    Coefficient of Variation: {}".format(mMax[10],np.sqrt(float(mMax[12]))/float(mMax[10]))

    print "Experiment with minimum slope: ", mMin[:4]
    print "Slope: {}    Coefficient of Variation: {}".format(mMin[10],np.sqrt(float(mMin[12]))/float(mMin[10]))

    Ztest = (float(mMax[10])-float(mMin[10]))/np.sqrt(float(mMax[12]) + float(mMax[12]))
    #Ztest = (float(mMax[10])-float(mMin[10]))/np.sqrt(float(mMax[12])/float(mMax[14]) + float(mMax[12])/float(mMin[14])) # I think variance already includes # of data points - double check
    print "paired Z-value: ", Ztest

    # Intercept comparison
    print "**********************\nIntercept Z-test"
    # Find max and min slopes
    for entry in statlist:
        if entry[11] == max(statlist[:,11]): mMax = entry
        if entry[11] == min(statlist[:,11]): mMin = entry
    
    print "Experiment with maximum intercept: ", mMax[:4]
    print "Intercept: {}    Coefficient of Variation: {}".format(mMax[11],np.sqrt(float(mMax[13]))/float(mMax[11]))

    print "Experiment with minimum intercept: ", mMin[:4]
    print "Intercept: {}    Coefficient of Variation: {}".format(mMin[11],np.sqrt(float(mMin[13]))/float(mMin[11]))

    Ztest = (float(mMax[10])-float(mMin[10]))/np.sqrt(float(mMax[12]) + float(mMax[12]))
    print "paired Z-value: ", Ztest
    '''

    # Some details of our fitting process
    # http://www2.mpia-hd.mpg.de/~robitaille/PY4SCI_SS_2014/_static/15.%20Fitting%20models%20to%20data.html
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html

    # ANCOVA
    # SLOPES
    # Compute the SUM OF SQUARES BETWEEN TREATMENTS (EXPERIMENTS)
    # The mean SSBTr indicates magnitude of difference between experiments
    # Parameters needed:
    # Slope: 10
    # Slope variance: 12
    # Number of points: 14

    print "\n**************************\nANCOVA MB slope\n**************************"
    # Grand mean slope
    statlist = np.asarray(statlist)
    totalpoints = 0
    for i in statlist[:,14]: totalpoints = totalpoints + int(i)
    grandSlopeSum = 0.0
    for experiment in statlist:
        grandSlopeSum = grandSlopeSum + float(experiment[14])*float(experiment[10])
    grandMeanSlope = grandSlopeSum/totalpoints
    #print "Grand slope sum ", grandSlopeSum
    #print "Total points ", totalpoints
    #print "Grand mean slope ", grandSlopeSum/totalpoints 

    # Finally, get sum of squares between treatments
    SSBetween = 0.0
    for experiment in statlist:
        SSBetween = SSBetween + float(experiment[14])*math.pow(float(experiment[10])-grandMeanSlope,2)
    SSB_df = len(statlist)-1 # Degrees of freedom for sum of squares between treatments is # treatments - 1

    print "SS between: ", SSBetween, "DF for SSB: ", SSB_df, "Mean SSB: ", SSBetween / SSB_df

    # Compute the SUM OF SQUARES WITHIN TREATMENTS (EXPERIMENTS)
    # The mean SSW indicates the magnitude of the variation in the data taken as a whole
    SSWithin = 0.0
    for experiment in statlist:
        SSWithin = SSWithin + (float(experiment[14])-1)*float(experiment[12])
    SSW_df = totalpoints - len(statlist) # Degrees of freedom for sum of squares between treatments
    print "SS within: ", SSWithin, "DF for SSW: ", SSW_df, "Mean SSW: ", SSWithin / SSW_df

    FtestSlope = (SSBetween / SSB_df)/(SSWithin / SSW_df)
    print "F-test value (mSSB/mSSW): ", FtestSlope

    confidence = 0.1
    print "Computing F-distribution P-value with confidence {}, SSB df {}, and SSW df {}".format(confidence,SSB_df,SSW_df)
    pSlope = f.sf(FtestSlope,SSB_df,SSW_df)
    print "P value (odds varition within tests accounts for variation between tests): ", pSlope

    # PLOT DATA FOR COMPARISONS
    fontP = FontProperties()
    fontP.set_size('small')
    
    plt.legend(numpoints = 2,loc='upper center', ncol=2, prop = fontP, bbox_to_anchor=(0.5, 1.0))
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Stress (MPa)")
    #plt.ylim([0,2])
    plt.savefig("PCplot.jpg")
    plt.show() 
    return "PLOTS CLOSED" 


start = time.clock()
print Plot_PC("PC",material[matnum])
print "Analysis time: ", time.clock()-start


