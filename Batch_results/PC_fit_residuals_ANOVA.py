# Written by Wade Lanning July 2017
# This program is for fitting curves to plastic collapse data sets and looking at the residuals
# Residuals are the difference between a curve fit and the original data points
# Some helpful notes on looking at residuals
# http://docs.statwing.com/interpreting-residual-plots-to-improve-your-regression/


import xlrd # needed for xls files
from openpyxl.workbook import Workbook 
from openpyxl.reader.excel import load_workbook, InvalidFileException # For opening xlsx files
import openpyxl

import matplotlib.pyplot as plt
import matplotlib.cm as cm # For color mapping
from matplotlib.font_manager import FontProperties
from matplotlib.patches import Rectangle

# Import all the math functions we'll need
import math
import numpy as np
from scipy.optimize import curve_fit # Nonlinear least squares fitting
from scipy.stats.distributions import  t # For t-test stat comparisons
from scipy.stats import f # For f-test stat comparisons

import csv # For opening tab-delimited files

import time # for using dates in saved files
import os # for managing paths

# Pull in the various fits that we could use.
import Fits_and_analysis as fitfunc

# Lists of All of the original experiment parameters
# Materials
material = ["Al","Cu","Sn"]
matnum = 0 # Which material do we want to analyze?
# Level 1: Thickness
Althick = [25.4,50.8,127]
Cuthick = [50.8,76.2,127]
Snthick = [25,50,100]
thickness = [Althick,Cuthick,Snthick]
# Level 2: Specimen types
specimen = ["T","SE","M"]
# Level 3: Crack length
cracklength = [1.0,2.0]

# Here is where I trim things down if desired
#specimen = [specimen[1]]
#thickness = [Althick[2]]
#thickness = [Cuthick[1]]
thickness = [Snthick[0]]
cracklength = [1.0,2.0]
specimen = ["SE","M"]

# Set up configurations we want to look at
# This will make a list of the configurations we planned
SpecConfigList = []
ThicknessList = []
for thick in thickness:
    ThicknessList.append("B{}um".format(thick))
    for config in specimen:
        if config == "T": SpecConfigList.append("Tensile")
        else: 
            for length in cracklength: SpecConfigList.append("{0}{1}mm".format(config,length))
# Show what  specimen configurations or thicknesses are going to be used
# SpecConfigList = ["SE2.0mm", "M1.0mm"] # For comparing specimens that start at the same a/w
print "Configs: ",SpecConfigList
print "Thicknesses: ",ThicknessList

def turntolabel(list): # For turning lists of experiment information into data labels
    label = ''
    for item in list:
        label = label + str(item) + ' '
    return label

def Plot_PC(path,material): # Takes in path to batch analysis results file.
    # We might want to know when this analysis was run.
    analysistime = str(time.strftime("%d"))+time.strftime("%b")+str(time.strftime("%Y"))+"_"+str(time.strftime("%H"))+"."+str(time.strftime("%M"))+"."+str(time.strftime("%S"))

    plt.figure("PC fits and residuals",figsize=(20,16)) # Create a figure we will populate with our data

    xfit = np.linspace(0,1) # The range of x values for plotting each fit
    
    # Open the raw plastic collapse data
    PC_data = []
    with open("{}\{}.txt".format(path,material), 'r') as inputfile:
        reader = csv.reader(inputfile, delimiter='\t')
        for row in reader:
            PC_data.append(row)

    # We will combine all of the PROPAGATION data into one giant set
    awWholeSet = []
    stressWholeSet = []
    awDict = {} # Dictionary for aw data
    stressDict = {}

    awInitD = {} # Dictionary for discarded initiation data
    stressInitD = {}

    for experiment in xrange(0,len(PC_data)/3): # This steps through every recorded experiment, which is composed of 3 lines
        # Here we set up conditional statements to decide what data to look at
        if PC_data[experiment*3][1] in ThicknessList and PC_data[experiment*3][2] in SpecConfigList:
            if PC_data[experiment*3][2][0] == "T": pmark = "D"
            elif PC_data[experiment*3][2][0] == "M": pmark = "s"
            else: pmark = "o"

            awData, stressData = map(float,PC_data[experiment*3+1][1:]),map(float,PC_data[experiment*3+2][1:]) # Data has to be converted to floats

            if PC_data[experiment*3][2][0] != "T": 
                # Distinguish between initiation and propagation.
                for i in range(0, len(stressData)): # Separate the initiation portion of the curve from the propagation portion
                    if stressData[i] == max(stressData):            
                        CIDX = i
                        break
                if 'CIDX' not in locals() and 'CIDX' not in globals(): 
                    CIDX = 0 # In case propagation search somehow failed, send value so analysis can continue
                    print "PC analysis warning: initiation/propagation transition not found."

                awInit, stressInit = awData[:CIDX],stressData[:CIDX]
                awProp, stressProp = awData[CIDX:],stressData[CIDX:]

                datLabel = turntolabel(PC_data[experiment*3])
                # Add the data to the grand combined data set.
                awWholeSet = awWholeSet + awProp
                stressWholeSet = stressWholeSet + stressProp

                awDict[datLabel] = awProp # add individual experiment data to a library to use later in statistical analysis
                stressDict[datLabel] = stressProp

                awInitD[datLabel] = awInit # add individual experiment data to a library to use later in statistical analysis
                stressInitD[datLabel] = stressInit

            else: 
                #plt.scatter(PC_data[experiment*3+1][1:],PC_data[experiment*3+2][1:], c = colors[experiment], marker = pmark, label = PC_data[experiment*3])
                print PC_data[experiment*3+1][1:],PC_data[experiment*3+2][1:] 

    
    '''
    # awPower law
    def func(x, a, n): return a*(1-x**n)
    #fitParams = fitfunc.awPowFit(awWholeSet, stressWholeSet)
    '''
    ''' # Linear slope-intercept fit
    def func(x, m, b): return m * x + b
    #fitParams = fitfunc.mbFit(awWholeSet, stressWholeSet)
    '''
    ''' # pcPower law
    def func(x, a, n): return a*(1-x)**n
    #fitParams = fitfunc.pcPowFit(awWholeSet, stressWholeSet)
    '''
    '''
     # single-parameter linear fit
    def func(x, a): return a*(1-x)
    fitParams = fitfunc.m_fit(awWholeSet, stressWholeSet) 
    '''

    awTrunc = {} # Place where truncated aw data will be stored (a dictionary)
    stressTrunc = {} 

    minAWlist = sorted(awWholeSet) # This puts the aw values in order from low to high
    # Now we must step through this list using the values as a minimum aw and:
    # 1) Remove all data points with lower aw than listed here from individual experiments
    # 2) Combine the newly truncated datapoints into a master list
    # 3) Fit the individual datasets and the combined dataset and compute residuals
    # 4) Use the residuals to compute SoSW and SoST, and thus SoSB = SoST - SoSW and do an ANOVA-type comparison

    # The truncated datasets will change in size, so we shall iterate through the full datasets and rebuild the truncated ones every time
    subAW, subStress, subF, subP = [], [], [], []
    subSoSB, subSoSW = [], []
    
    for minAW in minAWlist[:-1]: 
        awComb = [] # Master lists for combined data
        stressComb = []
        for key in awDict: 
            awTrunc[key] = [] # First empty out the place where we'll keep truncated datasets
            stressTrunc[key] = []
            for i in xrange(0,len(awDict[key])): # Iterate positions of data inside of awDict[key] lists so we can call from stressDict too
                if awDict[key][i] >= minAW: 
                    # Add qualifying data point to both individual and combined data lists
                    awTrunc[key].append(awDict[key][i])
                    awComb.append(awDict[key][i])
                    stressTrunc[key].append(stressDict[key][i])
                    stressComb.append(stressDict[key][i])    
            if len(awTrunc[key]) <= 0:
                print "{} ran out of points aw={}.".format(key,minAW)
                break
                break
                
        # At this point, both the individual and combined truncated datasets are built
        # Time to fit each of them then do an ANOVA-type analysis
        # This requires computation of the sums of square and degrees of freedom from all the residuals
         
        SoSWtrunc = 0
        SoSTtrunc = 0
        #try:
        for key in awTrunc:
            # Fit awTrunc
            #try: # Fitting operations sometimes fail. Using 'try' will allow the program to continue even if that happens.
            try: fitParams = fitfunc.awPowFit(awTrunc[key], stressTrunc[key]) # aw power law fit
            except: 
                try:
                    #print key,"Fit failure with points: ", len(awTrunc[key])
                    fitParams = fitfunc.m_fit(awTrunc[key], stressTrunc[key]) # single-parameter linear fit                    
                except: 
                    #print "Linear fit failed, data points: ", len(awTrunc[key])
                    fitparams = {}
                    fitparams["residuals"] = []
            # Compute SoSW from residuals and add to total
            for value in fitParams["residuals"]: SoSWtrunc = SoSWtrunc + value**2
            #except: print "Fitting failure of {} with {} points.".format(key,len(awTrunc[key])

        # Fit awComb
        try:
            fitParams = fitfunc.awPowFit(awComb, stressComb) # aw power law fit
            #fitParams = fitfunc.m_fit(awComb, stressComb) # single-parameter linear fit
            # Compute SoST from fit
            for value in fitParams["residuals"]: SoSTtrunc = SoSTtrunc + value**2
            # Compute SoSB from SoST - SoSW
            SoSBtrunc = SoSTtrunc - SoSWtrunc
            # Compute DOF for SoSB and SoSW
            SoSBdf = len(awComb) - 1
            SoSWdf = (len(awComb) - len(awDict))
            # Compute mSoSB and mSOSW
            mSoSB = SoSBtrunc / SoSBdf
            mSoSW = SoSWtrunc / SoSWdf
            subSoSB.append(mSoSB)
            subSoSW.append(mSoSW)
            # Compute F-value
            Ftrunc = mSoSB / mSoSW
            # Compute P-value
            Ptrunc = f.sf(Ftrunc,SoSBdf,SoSWdf)

            
            subAW.append(minAW) # Minimum aw for the subset is important for defining region of convergence
            subStress.append(max(stressComb)) # This is just for later use when displaying the range of convergence
            subF.append(Ftrunc) # A list of F-values for the subsets
            subP.append(Ptrunc) # A list of P-values for the subsets, used to define convergence
        except: print "Combined data fitting failure: {} points".format(len(awComb))

    # Finding crack length where convergence defined by p-value occurs
    awConverge = 1
    stressConverge = 0
    confidence = 0.01
    for i in xrange(0,len(subP)): 
        if subP[i] >= confidence: # If the P-value exceeds our confidence level
            awConverge = subAW[i] # Record the critical convergence crack length
            stressConverge = subStress[i] # Record the maximum stress of the data subset which showed convergence
            break # Get out of the loop otherwise we'll overwrite the value we found 
    ConRecX, ConRecY = [awConverge,awConverge,1,1,awConverge], [stressConverge,0,0,stressConverge,stressConverge] # For drawing a rectangle around converged region later

    #### DISPLAY FITS AND DATA OF THE NON-TRUNCATED DATASET ####
        # Setting up plotting color coding
    #colors = cm.gist_ncar(np.linspace(0, 1, len(awDict))) # For large datasets - no duplicate colors
    colors = cm.prism(np.linspace(0, 1, len(awDict))) # For small datasets - high contrast but may duplicate
    colors = cm.nipy_spectral(np.linspace(0, 3, len(PC_data)/3)) # muted rainbow colors - appropriate for large datasets
    #colors = cm.flag(np.linspace(0, 1, len(awDict))) # For true patriots
    c = 0
    s = 36 # Marker size

    SoSwithin = 0 # Sum of square within for ALL experiments combined
    residWithin = []

    # Fit and show individual experiments (non-truncated)
    for expname in awDict:
        if "SE" in expname: pmark = "o" # Mark SE with circle
        else: pmark = "s" # Mark all others (presumably M(T) with squares)
        # Plot the data, fit the data, and show the fit
        plt.subplot(2,2,1)
        plt.scatter(awDict[expname], stressDict[expname], c = colors[c], marker = pmark, s=s, edgecolors='none', label = expname) # Plot experiment
        fitParams = fitfunc.awPowFit(awDict[expname], stressDict[expname]) # Fit experiment
        #fitParams = fitfunc.m_fit(awDict[expname], stressDict[expname]) 

        plt.plot(fitParams["xfit"],fitParams["yfit"], linewidth=2, c = colors[c])  

        # Plot residuals
        plt.subplot(2,2,3)
        plt.scatter(awDict[expname], fitParams["residuals"], c = colors[c], marker = pmark, s=s, edgecolors='none', label = expname) 
        residWithin = residWithin + fitParams["residuals"]

        c = c+1        
        
        for value in fitParams["residuals"]: SoSwithin = SoSwithin + value**2 # Sum of square residuals is equivalent to sum of squares within treatment

    SoSW_df = (len(awWholeSet)-len(awDict)) # Degrees of freedom for sum of squares within
    mSoSW = SoSwithin / SoSW_df # Mean of sum of squares within

    # WHOLE SET ANOVA
    SoSwithin = 0
    SoStotal = 0
    for key in awDict:
        # Fit awTrunc
        fitParams = fitfunc.awPowFit(awDict[key], stressDict[key]) # aw power law fit
        #fitParams = fitfunc.m_fit(awDict[key], stressDict[key]) # single-paremeter linear fit
        # Compute SoSW from residuals and add to total
        for value in fitParams["residuals"]: SoSwithin = SoSwithin + value**2
    # Fit combined data
    fitParams = fitfunc.awPowFit(awWholeSet, stressWholeSet) # aw power law fit
    #fitParams = fitfunc.m_fit(awWholeSet, stressWholeSet) # single-parameter linear fit
    # Compute SoST from fit
    for value in fitParams["residuals"]: SoStotal = SoStotal + value**2
    # Compute SoSB from SoST - SoSW
    SoSbetween = SoStotal - SoSwithin
    # Compute DOF for SoSB and SoSW
    SoSbetween_df = len(awDict)-1                    
    SoSwithin_df = (len(awWholeSet)-len(awDict))
    # Compute mSoSB and mSOSW
    mSoSbetween = SoSbetween / SoSbetween_df
    mSoSwithin = SoSwithin / SoSwithin_df

    # Compute F-value
    fWholeSet = mSoSbetween / mSoSwithin
    # Compute p-value (odds hypothesis that individual experiments DO NOT DIFFER is true)
    p_value = f.sf(fWholeSet,SoSbetween_df,SoSwithin_df)

    print "\n**************************\nANOVA of residuals\n**************************"
    print "Comparing fits of ",len(awDict) ,"experiments with ",len(awWholeSet) ,"total measurements."
    print "SoS between: ", SoSbetween, "DF for SoSB: ", SoSbetween_df, "Mean SoSB: ", mSoSbetween
    print "SoS within: ", SoSwithin, "DF for SoSW: ", SoSwithin_df, "Mean SoSW: ", mSoSwithin
    print "F-test value (mSoSB/mSoSW): ", fWholeSet

    print "F-distribution P-value with SSB df {} and SSW df {}".format(SoSbetween_df,SoSwithin_df)
    print "P value (odds random error accounts for variation between tests): ", p_value


    ##### PLOT RESIDUALS OF WHOLE SET AND ANOVA OF TRUNCATED SETS #######
    # Plot settings
    fontP = FontProperties()
    fontP.set_size('small')
    plt.subplot(2,2,1)
    plt.title("Individual Fits                        .")
    plt.legend(numpoints = 2,loc='upper left', ncol=1, prop = fontP, bbox_to_anchor=(0.5, 1.2))
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Stress (MPa)")

    #### GLOBAL FIT AND COMPUTATION OF THE SUM OF SQUARES BETWEEN ####    
    plt.subplot(2,2,2)
    plt.title("Combined Fit")    
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Nominal stress (MPa)")
    plt.scatter(awWholeSet, stressWholeSet, s=s, edgecolors='none')
    plt.plot(ConRecX,ConRecY,color='red',label="confidence level {}\nconvergence at a/w > {}".format(confidence,awConverge)) # Draw rectangle around region of convergence
    plt.legend()
    #plt.add_patch(Rectangle((0.5 - .1, 0.5 - .1), 0.2, 0.2, fill=None, alpha=1))

    # single-parameter linear fit
    #fitParams = fitfunc.m_fit(awWholeSet, stressWholeSet) # Single-parameter linear fit
    fitParams = fitfunc.awPowFit(awWholeSet, stressWholeSet) # power fit
    plt.plot(fitParams["xfit"],fitParams["yfit"], linewidth=2)  

    #### Plot of individual residuals ####    
    plt.subplot(2,2,3)
    plt.title("Residuals of individual fits")
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Residuals (MPa)")

    #### Plot of global residuals ####    
    plt.subplot(2,2,4)
    plt.title("Residuals of global fit")
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Residuals (MPa)")
    plt.scatter(awWholeSet, fitParams["residuals"], s=s, edgecolors='none', label = expname) 
    plt.plot(ConRecX,[2,-2,-2,2,2],color='red',label="confidence level {}\nconvergence at a/w > {}".format(confidence,awConverge)) # Draw rectangle around region of convergence

    #plt.savefig("Residuals_PCfit.jpg")

    plt.figure("ANOVA of residuals",figsize=(20,12))
    #plt.close('all')
    plt.subplot(2,2,1)
    plt.title("{} micron thick {}\n {}, a = {}".format(thickness, material, specimen, cracklength))
    plt.scatter(subAW,subSoSB, c = [1,1,0,1], label = "mean SoS between")
    plt.scatter(subAW,subSoSW, c = [1,0,1,1], label = "mean SoS within")
    plt.xlabel("Minimum crack length of dataset a/w")
    plt.ylabel("Sum of squares of residuals (MPa^2)")
    plt.xlim((0,1))
    plt.legend(numpoints = 2)

    plt.subplot(2,2,2)
    plt.title("F = mSoSB/mSoSW")

    plt.scatter(subAW,subF)
    plt.xlabel("Minimum crack length of dataset a/w")
    plt.ylabel("F-value")
    plt.xlim((0,1))

    plt.subplot(2,2,4)
    plt.title("P-value")
    plt.scatter(subAW,subP)
    plt.plot([0,1],[confidence,confidence], color="red") # Shows confidence level
    plt.xlabel("Minimum crack length of dataset a/w")
    plt.ylabel("P-value")
    plt.xlim((0,1))
    plt.ylim((0,0.25))
    #plt.savefig("Residual_ANOVA.jpg")


    #plt.show()
   
    # PUBLICATION-WORTHY PLOTS

    # INDIVIDUAL EXPERIMENTS
    # Fit and show individual experiments (non-truncated)
    plt.close('all')
    plt.figure("Individual experiments with power law fit lines", figsize = (18,6))    
    plt.subplot(1,2,1) 
    s = 36 # Marker size

    c = 0
    for expname in awDict:
        if "SE" in expname: pmark = "o" # Mark SE with circle
        else: pmark = "s" # Mark all others (presumably M(T) with squares)  
        fitParams = fitfunc.awPowFit(awDict[expname], stressDict[expname]) # Fit experiment
        #fitParams = fitfunc.m_fit(awDict[expname], stressDict[expname]) 
        c = c + 1
        plt.plot(fitParams["xfit"],fitParams["yfit"], linewidth=2, c = colors[c], zorder=2)  
        plt.scatter(awDict[expname], stressDict[expname], marker = pmark, c = colors[c], s=s,  label = expname, zorder=3) # Plot propagation data
        plt.scatter(awInitD[expname], stressInitD[expname], marker = pmark, c = colors[c], edgecolors='none', s=s, zorder=1) # Plot initiation data


    fontP = FontProperties()
    fontP.set_size('small')
    plt.subplot(1,2,1)
    plt.title("Stress correlation analysis for B =  {} um".format(thickness[0]))
    plt.legend(numpoints = 2,loc='upper left', ncol=1, prop = fontP, bbox_to_anchor=(1.05, 1.0))
    plt.xlim((0,1))
    plt.ylim((0,15))
    plt.xlabel("Normalized crack length\na/w for SE(T) or 2a/w for M(T)")
    plt.ylabel("Stress (MPa)")
    #plt.tick_params(labelsize=10)
    #plt.savefig("PCplot.eps")


    ##### DO A LINEAR FIT TO THE CONVERGED SUBSET
    awCsub, stressCsub = [], []
    for key in awDict: 
        for i in xrange(0,len(awDict[key])): # Iterate positions of data inside of awDict[key] lists so we can call from stressDict too
            if awDict[key][i] >= awConverge: 
                # Add qualifying data point to both individual and combined data lists
                awCsub.append(awDict[key][i])
                stressCsub.append(stressDict[key][i])  
    fitParams = fitfunc.m_fit(awCsub, stressCsub) # single-paremeter linear fit
    # Find STDV of residuals
    rSOS = 0
    for resid in fitParams["residuals"]: rSOS = rSOS + resid**2
    rSTDV = (rSOS / (len(fitParams["residuals"])-1))**0.5

    print "CONVERGENCE FOUND AT a/w = ", min(awCsub), " for B = ",thickness[0]
    print "Linear fit found with slope ",fitParams["mFit m"]," MPa and STDV ",rSTDV

    plt.figure("Converged data subset with linear fit.", figsize = (12,8))
    before = plt.scatter(awWholeSet,stressWholeSet,color = "red", label = "Before convergence", zorder = 1)
    after = plt.scatter(awCsub,stressCsub,color = "blue", label = "After convergence", zorder = 3)
    cfit = plt.plot(fitParams["xfit"],fitParams["yfit"], label = "m = {} MPa".format(fitParams["mFit m"]), zorder = 2)
    plt.title("Converged data subset with linear fit.\n{} micron thick {}\n {}, a = {}".format(thickness[0], material, specimen, cracklength))
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Stress (MPa)")
    plt.legend()
    plt.xlim((0,1))
    plt.ylim((0,15))
    #plt.savefig("converged_linear_fit.eps")

    plt.show()


    # NET SECTION STRESS REPRESENTATION
    plt.close("all")
    plt.figure("Net section stress plot", figsize = (12,8))
    c = 0
    for expname in awDict:
        if "SE" in expname: pmark = "o" # Mark SE with circle
        else: pmark = "s" # Mark all others (presumably M(T) with squares)  
        nss = []
        for i in xrange(0,len(awDict[expname])): nss.append(stressDict[expname][i] / (1-awDict[expname][i]))
        #fitParams = fitfunc.m_fit(awDict[expname], stressDict[expname]) 
        c = c + 1
        plt.scatter(awDict[expname], nss, marker = pmark, c = colors[c], s=s,  label = expname, zorder=3) # Plot net section stress data

    plt.title("Net section stress analysis\n{} micron thick {}\n {}, a = {}".format(thickness[0], material, specimen, cracklength))
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Net section stress (MPa)")
    plt.legend(numpoints = 2,loc='upper left', ncol=1, prop = fontP, bbox_to_anchor=(1.0, 1.0))
    plt.xlim((0,1))
    #plt.ylim((0,70))

    #plt.show()




    # Some details of our fitting process
    # http://www2.mpia-hd.mpg.de/~robitaille/PY4SCI_SS_2014/_static/15.%20Fitting%20models%20to%20data.html
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
    return "Analysis completed."

start = time.clock()
print Plot_PC("PC",material[matnum])
print "Analysis time: ", time.clock()-start


