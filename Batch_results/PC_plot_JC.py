import xlrd # needed for xls files
from openpyxl.workbook import Workbook 
from openpyxl.reader.excel import load_workbook, InvalidFileException # For opening xlsx files
import openpyxl

import matplotlib.pyplot as plt
import matplotlib.cm as cm # For color mapping
from matplotlib.font_manager import FontProperties

import numpy as np
from scipy.optimize import curve_fit
from scipy.stats.distributions import  t

import csv # For opening tab-delimited files

import time # for using dates in saved files
import os # for managing paths

# Lists of All of the original experiment parameters
# Materials
material = ["Al","Cu","Sn"]
matnum = 2 # Which material do we want to analyze?
# Level 1: Thickness
Althick = [25.4,50.8,127]
Cuthick = [50.8,76.2,127]
Snthick = [25,50,100]
thickness = [Althick,Cuthick,Snthick]
# Level 2: Specimen types
specimen = ["T","SE","M"]
# Level 3: Crack length
cracklength = [1.0,2.0]

# Here is where I trim things down if desired
#specimen = [specimen[1]]
Snthick = [Snthick[0]]
cracklength = [1.0,2.0]
specimen = ["T","SE","M"]

# Set up configurations we want to look at
# This will make a list of the configurations we planned
SpecConfigList = []
ThicknessList = []
for thickness in Snthick:
    ThicknessList.append("B{}um".format(thickness))
    for config in specimen:
        if config == "T": SpecConfigList.append("Tensile")
        else: 
            for length in cracklength: SpecConfigList.append("{0}{1}mm".format(config,length))
# Show what  specimen configurations or thicknesses are going to be used
print "Configs: ",SpecConfigList
print "Thicknesses: ",ThicknessList


def Plot_PC(path,material): # Takes in path to xlsx file. Does collapse analysis, saves fig and data at path, returns data
    # We might want to know when this analysis was run.
    analysistime = str(time.strftime("%d"))+time.strftime("%b")+str(time.strftime("%Y"))+"_"+str(time.strftime("%H"))+"."+str(time.strftime("%M"))+"."+str(time.strftime("%S"))

    plt.figure(figsize=(16,10)) # Create a figure we will populate with our data

    FIT_data = []
    with open("{}\PCresults.txt".format(path), 'r') as inputfile:
        reader = csv.reader(inputfile, delimiter='\t')
        for row in reader:
            FIT_data.append(row)
    print "Data loaded from fits."
    print FIT_data.pop(0) # This shows the user the column headers and removes them from the list.
    def func(x, m, b): # linear slope-intercept function
        return m * x + b
    xfit = np.linspace(0,1) # The range of x values for plotting each fit

    # We need to match the raw data to the fits. 
    # I will make a nested list. [ [material, thickness, spectype, name]   ,[fitting data]   ] 
    fitlist = []
    for item in FIT_data: fitlist.append([[item[:4]],[item[4:] ]])
    
    # Open the raw plastic collapse data
    PC_data = []
    with open("{}\{}.txt".format(path,material), 'r') as inputfile:
        reader = csv.reader(inputfile, delimiter='\t')
        for row in reader:
            PC_data.append(row)

    #colors = cm.jet(np.linspace(0, 1, len(PC_data)/3)) # For large datasets - no duplicate colors
    colors = cm.prism(np.linspace(0, 1, len(PC_data)/3)) # For small datasets - high contrast but may duplicate
    #colors = cm.flag(np.linspace(0, 1, len(PC_data)/3)) # For true patriots
    for experiment in xrange(0,len(PC_data)/3): # This steps through every recorded experiment, which is composed of 3 lines
        # Here we set up conditional statements to decide what data to look at
        if PC_data[experiment*3][1] in ThicknessList and PC_data[experiment*3][2] in SpecConfigList:
            if PC_data[experiment*3][2][0] == "T": pmark = "D"
            elif PC_data[experiment*3][2][0] == "M": pmark = "s"
            else: pmark = "o"
            plt.scatter(PC_data[experiment*3+1][1:],PC_data[experiment*3+2][1:], c = colors[experiment], marker = pmark, label = PC_data[experiment*3])
            
            for fitItem in FIT_data: # Find the row in the fit data which matches the data of interest, then plot the corresponding line.
                if PC_data[experiment*3][0] in fitItem and PC_data[experiment*3][1] in fitItem and PC_data[experiment*3][2] in fitItem and PC_data[experiment*3][3] in fitItem and "N/A" not in fitItem:
                    yfit = func(xfit, float(fitItem[10]), float(fitItem[11]))
                    print fitItem[:4]
                    print "Actual thickness: ", fitItem[4]
                    print "m:", fitItem[10]
                    print "b:", fitItem[11]
                    print "mvar:", fitItem[12]
                    print "bvar:", fitItem[13]
                    plt.plot(xfit,yfit, c = colors[experiment])

    fontP = FontProperties()
    fontP.set_size('small')
    
    plt.legend(numpoints = 2,loc='upper center', ncol=2, prop = fontP, bbox_to_anchor=(0.5, 1.0))
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Stress (MPa)")
    #plt.ylim([0,2])
    plt.savefig("PCplot_JC.jpg")
    plt.show() 
    return "PLOTS CLOSED" 


start = time.clock()
print Plot_PC("PC",material[matnum])
print "Analysis time: ", time.clock()-start


