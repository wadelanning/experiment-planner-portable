# Written by Wade Lanning July 2017
# This program is for fitting curves to plastic collapse data sets and looking at the residuals
# Residuals are the difference between a curve fit and the original data points
# Some helpful notes on looking at residuals
# http://docs.statwing.com/interpreting-residual-plots-to-improve-your-regression/


import xlrd # needed for xls files
from openpyxl.workbook import Workbook 
from openpyxl.reader.excel import load_workbook, InvalidFileException # For opening xlsx files
import openpyxl

import matplotlib.pyplot as plt
import matplotlib.cm as cm # For color mapping
from matplotlib.font_manager import FontProperties

# Import all the math functions we'll need
import math
import numpy as np
from scipy.optimize import curve_fit # Nonlinear least squares fitting
from scipy.stats.distributions import  t # For t-test stat comparisons
from scipy.stats import f # For f-test stat comparisons

import csv # For opening tab-delimited files

import time # for using dates in saved files
import os # for managing paths

# Pull in the various fits that we could use.
import Fits_and_analysis as fitfunc

# Lists of All of the original experiment parameters
# Materials
material = ["Al","Cu","Sn"]
matnum = 0 # Which material do we want to analyze?
# Level 1: Thickness
Althick = [25.4,50.8,127]
Cuthick = [50.8,76.2,127]
Snthick = [25,50,100]
thickness = [Althick,Cuthick,Snthick]
# Level 2: Specimen types
specimen = ["T","SE","M"]
# Level 3: Crack length
cracklength = [1.0,2.0]

# Here is where I trim things down if desired
#specimen = [specimen[1]]
Althick = [Althick[0]]
#Snthick = [Snthick[2]]
cracklength = [1.0,2.0]
specimen = ["SE","M"]

# Set up configurations we want to look at
# This will make a list of the configurations we planned
SpecConfigList = []
ThicknessList = []
for thickness in Althick:
    ThicknessList.append("B{}um".format(thickness))
    for config in specimen:
        if config == "T": SpecConfigList.append("Tensile")
        else: 
            for length in cracklength: SpecConfigList.append("{0}{1}mm".format(config,length))
# Show what  specimen configurations or thicknesses are going to be used
print "Configs: ",SpecConfigList
print "Thicknesses: ",ThicknessList


def Plot_PC(path,material): # Takes in path to batch analysis results file.
    # We might want to know when this analysis was run.
    analysistime = str(time.strftime("%d"))+time.strftime("%b")+str(time.strftime("%Y"))+"_"+str(time.strftime("%H"))+"."+str(time.strftime("%M"))+"."+str(time.strftime("%S"))

    plt.figure(figsize=(20,8)) # Create a figure we will populate with our data

    xfit = np.linspace(0,1) # The range of x values for plotting each fit
    
    # Open the raw plastic collapse data
    PC_data = []
    with open("{}\{}.txt".format(path,material), 'r') as inputfile:
        reader = csv.reader(inputfile, delimiter='\t')
        for row in reader:
            PC_data.append(row)

    # We will combine all of the PROPAGATION data into one giant set
    awWholeSet = []
    stressWholeSet = []
    awDict = {} # Dictionary for aw data
    stressDict = {}

    # Setting up plotting color coding
    #colors = cm.gist_ncar(np.linspace(0, 1, len(PC_data)/3)) # For large datasets - no duplicate colors
    colors = cm.prism(np.linspace(0, 1, len(PC_data)/3)) # For small datasets - high contrast but may duplicate
    #colors = cm.flag(np.linspace(0, 1, len(PC_data)/3)) # For true patriots
    plt.subplot(1,2,1)

    for experiment in xrange(0,len(PC_data)/3): # This steps through every recorded experiment, which is composed of 3 lines
        # Here we set up conditional statements to decide what data to look at
        if PC_data[experiment*3][1] in ThicknessList and PC_data[experiment*3][2] in SpecConfigList:
            if PC_data[experiment*3][2][0] == "T": pmark = "D" # Tensile tests marked as diamons
            elif PC_data[experiment*3][2][0] == "M": pmark = "s" # M(T) tests marked as squares
            else: pmark = "o" # Everything else (meanins SE(T)) is a circle

            awData, stressData = map(float,PC_data[experiment*3+1][1:]),map(float,PC_data[experiment*3+2][1:]) # Data has to be converted to floats

            if PC_data[experiment*3][2][0] != "T": 
                # Distinguish between initiation and propagation.
                for i in range(0, len(stressData)): # Separate the initiation portion of the curve from the propagation portion
                    if stressData[i] == max(stressData):            
                        CIDX = i
                        break
                if 'CIDX' not in locals() and 'CIDX' not in globals(): 
                    CIDX = 0 # In case propagation search somehow failed, send value so analysis can continue
                    print "PC analysis warning: initiation/propagation transition not found."

                awInit, stressInit = awData[:CIDX],stressData[:CIDX]
                awProp, stressProp = awData[CIDX:],stressData[CIDX:]

                s = 36 # Marker size
                plt.scatter(awInit, stressInit, c = colors[experiment], marker = pmark, s=s, edgecolors='none', label = PC_data[experiment*3])
                plt.scatter(awProp, stressProp, c = colors[experiment], marker = pmark, s=s)

                # Add the data to the grand combined data set.
                awWholeSet = awWholeSet + awProp
                stressWholeSet = stressWholeSet + stressProp

                awDict[str(PC_data[experiment*3])] = awProp # add individual experiment data to a library to use later in statistical analysis
                stressDict[str(PC_data[experiment*3])] = stressProp

            else: 
                plt.scatter(PC_data[experiment*3+1][1:],PC_data[experiment*3+2][1:], c = colors[experiment], marker = pmark, label = PC_data[experiment*3])
                print PC_data[experiment*3+1][1:],PC_data[experiment*3+2][1:]
            
            # Add the data to the grand combined data set.
            awWholeSet = awWholeSet + awProp
            stressWholeSet = stressWholeSet + stressProp
 

    #''' # awPower law
    def func(x, a, n): return a*(1-x**n)
    fitParams = fitfunc.awPowFit(awWholeSet, stressWholeSet)
    yfit = func(xfit, float( fitParams["awPow int"] ), float( fitParams["awPow exp"] )) # Create the fit line data
    plt.plot(xfit,yfit, linewidth=2)
    #''' 

    ''' # Linear slope-intercept fit
    def func(x, m, b): return m * x + b
    fitParams = fitfunc.mbFit(awWholeSet, stressWholeSet)
    yfit = func(xfit, float( fitParams["m"] ), float( fitParams["b"] )) # Create the fit line data
    plt.plot(xfit,yfit, linewidth=2) 
    '''

    ''' # pcPower law
    def func(x, a, n): return a*(1-x)**n
    fitParams = fitfunc.pcPowFit(awWholeSet, stressWholeSet)
    yfit = func(xfit, float( fitParams["pcPow int"] ), float( fitParams["pcPow exp"] )) # Create the fit line data
    plt.plot(xfit,yfit, linewidth=2) 
    '''

    ''' # awPower law
    def func(x, a, n): return a*(1-x**n)
    fitParams = fitfunc.awPowFit(awWholeSet, stressWholeSet)
    yfit = func(xfit, float( fitParams["awPow int"] ), float( fitParams["awPow exp"] )) # Create the fit line data
    plt.plot(xfit,yfit, linewidth=2)
    ''' 
   
    resMean = np.mean(fitParams["residuals"])
    #resVar = np.var(fitParams["residuals"]) # Not quite what we want since it is mean square deviation from the mean. We want deviation from the fit.
    #standErr = np.sqrt(resVar / len(fitParams["residuals"]) )
    SumSquares = 0 # The sum of squares of deviation from the fit are more useful.
    for value in fitParams["residuals"]: SumSquares = SumSquares + value**2 

    print "\n**************************\nResidual analysis\n**************************"
    print "Total data points: ", len(fitParams["residuals"])
    print "Mean of residuals: ", resMean
    #print "Variance of residuals: ", resVar 
    print "Sum of squares of residuals: ", SumSquares
    print "Meas square deviation from fit: ", SumSquares/(len(fitParams["residuals"])-1)
    
        

    fontP = FontProperties()
    fontP.set_size('small')
    plt.title("Plastic Collapse Plot")
    plt.legend(numpoints = 2,loc='lower left', ncol=1, prop = fontP, bbox_to_anchor=(0.0, 0.0))
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Stress (MPa)")

    plt.subplot(1,2,2)
    plt.title("Residuals")
    plt.xlabel("Normalized crack length a/w")
    plt.ylabel("Difference between fit and measurements (MPa)")
    plt.scatter(awWholeSet, fitParams["residuals"], c = colors[experiment], marker = pmark, s=s)

    plt.savefig("Residuals_PCfit.jpg")
    plt.show()
   
    # Some details of our fitting process
    # http://www2.mpia-hd.mpg.de/~robitaille/PY4SCI_SS_2014/_static/15.%20Fitting%20models%20to%20data.html
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
    return "Analysis completed."

start = time.clock()
print Plot_PC("PC",material[matnum])
print "Analysis time: ", time.clock()-start


